use std::io::prelude::*;
use super::app_infos;

pub struct BpmFile
{
    pub name: String,
    pub version: String,
    pub license: String,
    pub author: String,
    pub deps: String,
    pub checksum: String,
    pub build_dir: String,
    pub install_dir: String
}

impl BpmFile
{
    pub fn new(pkg_name : String) -> std::io::Result<BpmFile>
    {
	let mut result = BpmFile {
	    name : pkg_name.clone(),
	    version : String::from(""),
	    license : String::from(""),
	    author : String::from(""),
	    deps : String::from(""),
	    checksum : String::from(""),
	    build_dir : String::from(""),
	    install_dir : String::from("")
	};
	
	let file : std::fs::File = std::fs::File::open(app_infos::BPM_REPO_DIR.to_owned() + "/" + pkg_name.as_str() + ".bpm")?;
	let br : std::io::BufReader<std::fs::File> = std::io::BufReader::new(file);
	
	for l in br.lines()
	{
	    let line = l?;
	    
	    if line.replace("=", "").len() != line.len()
	    {
		let values : Vec<&str> = line.split("=").collect();

		if values[0].replace("VERSION", "").len() != values[0].len()
		{
		    result.version= String::from(values[1]);
		}

		if values[0].replace("LICENSE", "").len() != values[0].len()
		{
		    result.license= String::from(values[1]);
		}
		
		if values[0].replace("AUTHOR", "").len() != values[0].len()
		{
		    result.author= String::from(values[1]);
		}

		if values[0].replace("DEPS", "").len() != values[0].len()
		{
		    result.deps= String::from(values[1]);
		}

		if values[0].replace("CHECKSUM", "").len() != values[0].len()
		{
		    result.checksum= String::from(values[1]);
		}

		if values[0].replace("BUILD_DIR", "").len() != values[0].len()
		{
		    result.build_dir= String::from(values[1]);
		}

		if values[0].replace("INSTALL_DIR", "").len() != values[0].len()
		{
		    result.install_dir= String::from(values[1]);
		}
	    }
	}

	return Ok(result);
    }
}
