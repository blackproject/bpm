use super::bpm_file;
use super::app_infos;

use colored::*;

pub fn repo_install()
{
    let args : Vec<String> = std::env::args().collect();

    if args.len() < 3
    {
	eprintln!("Missing arguments.");
	super::print_help();
	std::process::exit(4);
    }
        
    install_package(args[2].clone())
}

pub fn repo_uninstall()
{
    let args : Vec<String> = std::env::args().collect();

    if args.len() < 3
    {
	eprintln!("Missing arguments.");
	super::print_help();
	std::process::exit(4);
    }
        
    uninstall_package(args[2].clone())
}

fn install_package(pkg_name : String)
{
    let bpm_res = bpm_file::BpmFile::new(pkg_name.clone());

    if let Err(_) = bpm_res
    {
	println!("{}", format!("Cannot find package {}.", &pkg_name).red());
	std::process::exit(8);
    }
    
    let bpm = bpm_res.unwrap();
    let path_bin = format!("{}/bin/{}", bpm.install_dir, bpm.name);
    let path_lib = format!("{}/lib/{}", bpm.install_dir, bpm.name);

    if std::path::Path::new(&path_bin).exists() || std::path::Path::new(&path_lib).exists()
    {
	println!("{}", format!("Package {} is already installed !", bpm.name).blue());
	std::process::exit(0);
    }
    
    let cmd : String = format!("make -f {}/{}.bpm", app_infos::BPM_REPO_DIR, bpm.name);
    
    let cmd_status = std::process::Command::new("sh")
	.arg("-c")
	.arg(cmd.as_str())
	.status()
	.unwrap();

    let cmd_clean : String = format!("rm -rf {}", bpm.build_dir);
    
    let cmd_clean_status = std::process::Command::new("sh")
	.arg("-c")
	.arg(cmd_clean.as_str())
	.status()
	.unwrap();

    if !cmd_status.success()
    {
	println!("{}", format!("Installation failed.").red());
	std::process::exit(7);
    }
    else
    {
	if !cmd_clean_status.success()
	{
	    println!("{}", format!("Post installation clean up failed.").red());
	    std::process::exit(8);
	}
	else
	{
	    println!("{}", format!("Installation success.").green());
	}
    }
}

fn uninstall_package(pkg_name : String)
{
    let bpm_res = bpm_file::BpmFile::new(pkg_name.clone());

    if let Err(_) = bpm_res
    {
	println!("{}", format!("Cannot find package {}.", &pkg_name).red());
	std::process::exit(9);
    }
    
    let bpm = bpm_res.unwrap();

    if !std::path::Path::new(&format!("{}/bin/{}", bpm.install_dir, bpm.name)).exists() &&
	!std::path::Path::new(&format!("{}/lib/{}", bpm.install_dir, bpm.name)).exists()
    {
	println!("{}", format!("Package {} is not installed !", bpm.name).blue());
	std::process::exit(0);
    }
    
    let cmd : String = format!("make -f {}/{}.bpm uninstall", app_infos::BPM_REPO_DIR, bpm.name);
    
    let cmd_status = std::process::Command::new("sh")
	.arg("-c")
	.arg(cmd.as_str())
	.status()
	.unwrap();

    if !cmd_status.success()
    {
	println!("{}", format!("Uninstallation failed.").red());
	std::process::exit(10);
    }
    else
    {
	println!("{}", format!("Uninstallation success.").green());
    }
}
