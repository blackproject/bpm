mod app_infos;
mod bpm_file;
mod update;
mod search;
mod install;
use std::io::prelude::*;

enum Command
{
    Invalid,
    Update,
    Search,
    Install,
    Uninstall,
    Info,
    Help,
    Version
}

pub fn run()
{
    let params : Vec<String> = std::env::args().skip(1).collect();

    if params.is_empty()
    {
	print_help();
	return;
    }

    let command : Command;

    match params[0].as_str()
    {
	"update" => { command = Command::Update; },
	"search" => { command = Command::Search; },
	"install" => { command = Command::Install; },
	"uninstall" => { command = Command::Uninstall; },
	"info" => { command = Command::Info; },
	"help" => { command = Command::Help; },
	"version" => { command = Command::Version; }
	_ => { command = Command::Invalid; }
    }

    execute(command);
}

fn execute(command : Command)
{
    match command
    {
	Command::Help => {
	    print_help();
	},
	
	Command::Version => {
	    print_version();
	},

	Command::Update => {
	    update_repo();
	},

	Command::Search => {
	    search_repo();
	},

	Command::Install => {
	    install::repo_install();
	},

	Command::Uninstall => {
	    install::repo_uninstall();
	},
	
	_ => { eprintln!("Not implemented yet."); }
    }
}

fn print_help()
{
    println!("Usage");
    println!("\tbpm update");
    println!("\tbpm search <package>");
    println!("\tbpm install <package>");
    println!("\tbpm uninstall <package>");
    println!("\tbpm info <package>");
    println!("\tbpm help");
    println!("\tbpm version");
}

fn print_version()
{
    println!("BPM version {}", app_infos::BPM_VERSION);
}

fn update_repo()
{
    if confirm("Do you really want to update your repo ? (Y/n)", "n")
    {
	update::update_repo();
    }
}

fn search_repo()
{
    search::search_repo();
}

fn confirm(msg : &str, cancel : &str ) -> bool
{
    println!("{}", msg);
    let mut rep : String = String::new();
    
    std::io::stdin().lock().read_line(&mut rep).unwrap();
    rep = rep.trim().to_string();
    rep = rep.to_lowercase();

    return !(rep == cancel);    
}
