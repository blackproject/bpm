use super::print_help;
use super::app_infos;
use super::bpm_file;
use colored::*;

pub fn search_repo()
{
    let args : Vec<String> = std::env::args().skip(2).collect();

    if args.is_empty()
    {
	eprintln!("Missing arguments");
	print_help();
	std::process::exit(3);
    }
    else
    {
	let package_name : &String = &args[0];	
	let dir = std::fs::read_dir(app_infos::BPM_REPO_DIR).unwrap();

	for entry in dir
	{
	    let file_name : String = entry.unwrap().file_name().into_string().unwrap();
	    let path = std::path::Path::new(& file_name);
	    
	    if let Some(_x) = path.extension()
	    {
		let name : Vec<&str> = path.to_str().unwrap().split(".").collect();

		if let Some(extension) = name.get(1)
		{
		    let get0 : &str = name.get(0).unwrap();
		    
		    if get0.replace(package_name, "").len() != get0.len() && extension == &"bpm"
		    {
			let bpm_file = bpm_file::BpmFile::new(String::from(get0)).unwrap();
			let version : String = bpm_file.version.clone();
			let license : String = bpm_file.license.clone();
			let author : String = bpm_file.author.clone();			
			    
			println!("{} - v{}, by {} - ({})", get0.white().on_black(), version.yellow(), author.blue(), license.green());
		    }
		}
	    }
	}
    }
}
