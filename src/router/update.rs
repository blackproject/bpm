use super::app_infos;
use colored::*;

pub fn update_repo()
{
    let repo_path : &std::path::Path = std::path::Path::new(app_infos::BPM_REPO_DIR);
    
    if !repo_path.exists()
    {
	if let Err(_x) = std::fs::create_dir_all(repo_path)
	{
	    eprintln!("{}", "Cannot create repo : are you root ?".red());
	    std::process::exit(1);
	}
	
	println!("Installing repo for the first time.");
	
	if let Err(_x) = std::process::Command::new("git")
	    .arg("clone")
	    .arg("https://gitlab.com/blackproject/repository.git")
	    .arg(app_infos::BPM_REPO_DIR)
	    .output()
	{
	    eprintln!("Cannot clone repo.");
	    std::process::exit(2);
	}
	
	println!("{}", "Repo successfully created.".green());
    }
    else
    {
	println!("Update repo.");
	if let Err(_x) = std::process::Command::new("sh")
	    .arg("-c")
	    .arg("cd ".to_owned() + app_infos::BPM_REPO_DIR + " && sudo git pull")
	    .status()
	{
	    eprintln!("{}", "Cannot pull repo.".red());
	    std::process::exit(2);
	}
	else
	{
	    println!("{}", "Repo successfully updated.".green());
	}
    }
    
}
